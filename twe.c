#include "cpuminer-config.h"
#include "miner.h"

#include <string.h>
#include <stdint.h>

#include "sph_fugue.h"
#include "sph_hamsi.h"
#include "sph_panama.h"
#include "sph_shavite.h"

inline void twehash(void *output, const void *input)
{
    unsigned char hash[128];

    memset(hash, 0, 128);

    sph_fugue256_context ctx_fugue;
    sph_shavite256_context ctx_shavite;
    sph_hamsi256_context ctx_hamsi;
    sph_panama_context ctx_panama;

    sph_fugue256_init(&ctx_fugue);
    sph_fugue256(&ctx_fugue, input, 80);
    sph_fugue256_close(&ctx_fugue, hash);

    sph_shavite256_init(&ctx_shavite);
    sph_shavite256(&ctx_shavite, hash, 64);
    sph_shavite256_close(&ctx_shavite, hash + 64);

    sph_hamsi256_init(&ctx_hamsi);
    sph_hamsi256(&ctx_hamsi, hash + 64, 64);
    sph_hamsi256_close(&ctx_hamsi, hash);

    sph_panama_init(&ctx_panama);
    sph_panama(&ctx_panama, hash, 64);
    sph_panama_close(&ctx_panama, hash + 64);

    memcpy(output, hash + 64, 32);
}

int scanhash_twe(int thr_id, uint32_t *pdata, const uint32_t *ptarget,
    uint32_t max_nonce, unsigned long *hashes_done)
{
    uint32_t n = pdata[19] - 1;
    const uint32_t first_nonce = pdata[19];
    const uint32_t Htarg = ptarget[7];

    uint32_t hash64[8] __attribute__((aligned(32)));
    uint32_t endiandata[32];

    //we need bigendian data...
    //lessons learned: do NOT endianchange directly in pdata, this will all proof-of-works be considered as stale from minerd.... 
    int kk=0;
    for (; kk < 32; kk++)
    {
	      be32enc(&endiandata[kk], ((uint32_t*)pdata)[kk]);
    };

    do {
	      pdata[19] = ++n;
	      be32enc(&endiandata[19], n); 
	      twehash(hash64, &endiandata);
            if (((hash64[7]&0xFFFFFF00)==0) && 
			      fulltest(hash64, ptarget)) {
                *hashes_done = n - first_nonce + 1;
		      return true;
	      }
    } while (n < max_nonce && !work_restart[thr_id].restart);

    *hashes_done = n - first_nonce + 1;
    pdata[19] = n;
    return 0;
}
